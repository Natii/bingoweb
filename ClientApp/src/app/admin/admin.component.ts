import { Component, OnInit } from '@angular/core';
import { SignalrcustomService } from '../service/signalrcustom.service';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Observable } from "rxjs/Observable";
import { Board } from '../_models/board'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { from } from 'rxjs';
import { Router } from '@angular/router'
import { UsersService } from "../_services/users.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {
 
  boards : Board[] = [];
  id: string;  
  public  boardsURL:string = 'https://localhost:5001/boards/';

  boardForm = new FormGroup({

    id : new FormControl('',Validators.required),
    NameBoard : new FormControl('',Validators.required)

  }) 

  constructor(private router : Router,public http: HttpClient,private userService: UsersService) {

   }

  ngOnInit() {
    
    this.refresh();
    this.id = localStorage.getItem('token'); 
    
    console.log(this.boards)
  }
  refresh(){
    
    this.http.get<Board[]>('https://localhost:5001/api/bingo/getBoards').subscribe(result => {
      this.boards = result;
    }, error => console.error(error));
    
  }
  generateBoards(form:Board){
    
    const result =this.boards.filter(p => p.id == form.id);
    if(result.length == 0 && form.id != null && form.NameBoard != ""){
       this.userService.addBoard(form).subscribe(data =>{ 
         
        this.boards.push(data);
        this.boardForm.reset();
      
      }, error => console.log(error.error));
    }else{
      console.log("esta en el arreglo");
    }
  }

  deleteBoard(board: Board){
    this.userService.delete(board);
    this.refresh();
  }

  logout() {  
    console.log('logout');  
    this.userService.logout();  
    this.router.navigate(['/login']);  
  }  
  

}
