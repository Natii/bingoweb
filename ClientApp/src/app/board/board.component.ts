import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { UsersService } from '../_services/users.service'
import { Board } from '../_models/board'
import { HubConnection, HubConnectionBuilder} from '@microsoft/signalr'
import {SignalrcustomService}  from '../service/signalrcustom.service'
import {BoardNumber} from '../_models/board.number'
import {BoardService} from '../_services/board.service'
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  boards : Board[] = [];
  id: number; 
  @Input() CurrentNumber:number;
  @Input() RecentNumbers: number[] = [];
  BoardData: Array<Array<BoardNumber>> = [];
  public numbers
  public hubConection:HubConnection;

  constructor(private lservice:BoardService,private route:ActivatedRoute,private adminBoard:UsersService,private service:SignalrcustomService) { 
    this.route.params.subscribe(params =>{
     
      var id = Number(params['id']);
      this.adminBoard.getBoard(id).subscribe(data =>{
        this.boards.push(data);
      })

    })

    
  }
  
  GenerateBoard(): Array<Array<BoardNumber>> {
    this.BoardData = [];
    for (let rowIndex = 0; rowIndex < 5; rowIndex++) {
      let row = new Array<BoardNumber>();
      for (let col = 1; col <= 5; col++) {
        let bNumber = new BoardNumber();
        bNumber.value = rowIndex * 15 + col;
        bNumber.selected = false;
        row.push(bNumber);
      }
      this.BoardData.push(row);
    }
    return this.BoardData;
  }

  
  ngOnInit() {

        this.service.emNotifica.subscribe(result =>{
          this.CurrentNumber = result;
         
          console.log(result)
          this.RecentNumbers.push(this.CurrentNumber);
          //this.RecentNumbers = JSON.parse(JSON.stringify(this.lservice.GetRecentNumbers(3)));
        })

        this.GenerateBoard();
  
  }

}