export class BoardNumber {
    id?: number;
    value: number;
    selected: boolean = false;
     url: string;
  }