import { Injectable } from '@angular/core';
import { HubConnection,HubConnectionBuilder } from '@microsoft/signalr'
import { EventEmitter } from '@angular/core';
import { Board } from '../_models/board';

@Injectable({
  providedIn: 'root'
})
export class SignalrcustomService {

  public hubConection: HubConnection;
  emNotifica: EventEmitter<number> = new EventEmitter();

  constructor() {
      let builder  = new HubConnectionBuilder();
      this.hubConection = builder.withUrl("https://localhost:5001/cnn").build();
      this.hubConection.on("Send", (message) => {
         let board : number = JSON.parse(message);
         this.emNotifica.emit(board);
      })

      this.hubConection.start()
   .then(()=>{console.log("Connection started ");})
   .catch(err =>{console.error(err);})
   }


}

