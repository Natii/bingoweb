import { Component, OnInit } from "@angular/core";
import { BoardNumber } from '../_models/board.number';
import { ActivatedRoute } from '@angular/router'
import {UsersService} from '../_services/users.service'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
 
  constructor(private service:UsersService) { 
    
 }
  BoardData: Array<Array<BoardNumber>> = [];

  GenerateBoard(): Array<Array<BoardNumber>> {
    this.BoardData = [];
    for (let rowIndex = 0; rowIndex < 5; rowIndex++) {
      let row = new Array<BoardNumber>();
      for (let col = 1; col <= 5; col++) {
        let bNumber = new BoardNumber();
        bNumber.value = rowIndex * 15 + col;
        bNumber.selected = false;
        row.push(bNumber);
      }
      this.BoardData.push(row);
    }
    return this.BoardData;
  }

  ngOnInit() {

      this.GenerateBoard();
  }
}
