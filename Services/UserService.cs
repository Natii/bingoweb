using bingoweb.Common;
using bingoweb.Models;
using bingoweb.IServices;
using System.Collections.Generic;
using BC = BCrypt.Net.BCrypt;
using System.Linq;


namespace bingoweb.Services 
{

    public class UserService : IUserService
    {
       

      public Administrator Login(Administrator oUser){

          var user = Global.Users.SingleOrDefault(x=>x.Email == oUser.Email);

          bool isValidPassword = BCrypt.Net.BCrypt.Verify(oUser.PasswordHash, user.PasswordHash);
          if(isValidPassword){
              return user;
          }

          return null;
      }   

       public Administrator Signup(Administrator oUser){
          
           oUser.PasswordHash = BCrypt.Net.BCrypt.HashPassword(oUser.PasswordHash);
           return oUser;
      }   
    }

}