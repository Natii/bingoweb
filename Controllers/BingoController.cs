using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using bingoweb.Models;
using bingoweb.IServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using bingoweb.hubs;
namespace bingoweb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BingoController :  ControllerBase
    {

        private readonly IHubContext<Message> _hubContext;
        private readonly DataBaseContext _context;
        public BingoController(IHubContext<Message> hubContext, DataBaseContext context)
        {
            _hubContext = hubContext;
            _context = context;

        }
        
        [HttpGet("getBoards")]
        public async Task<ActionResult<IEnumerable<Boards>>> getBoards()
        {
            return await _context.Boards.ToListAsync();
        }
        [HttpGet("getBoards/{id}")]
        public async Task<ActionResult<Boards>> getBoard(int id)
        {
            var board = await _context.Boards.FindAsync(id);
            if (board == null)
            {
                return NotFound();
            }
            return board;
        }

        [HttpPost("addBoard")]
        public async Task<ActionResult<Boards>> addBoard([FromBody] Boards boards)
        {
           
            _context.Boards.Add(boards);
            await _context.SaveChangesAsync();
            
            
            return Ok(boards);


        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Boards>> DeleteAnimal(int id)
        {
            var board = await _context.Boards.FindAsync(id);
            if (board == null)
            {
                return NotFound();
            }

            _context.Boards.Remove(board);
            await _context.SaveChangesAsync();

            return board;
        }



        
        [HttpGet("addNumber/{id}")]
        public async Task<ActionResult<Boards>> SendArticle(int id)
        {
          await _hubContext.Clients.All.SendAsync("Send", id);
          return Ok(new { resp = "the number was send successfully"});

        }
        

    }

}