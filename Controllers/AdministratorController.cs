using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using bingoweb.Models;
using bingoweb.IServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Admin.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdministratorController :  ControllerBase
    {
    
         private readonly DataBaseContext _context;
        IUserService _userService = null;
        public AdministratorController(IUserService userService, DataBaseContext context){
                _userService = userService;
                _context = context;
        }
       
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Administrator>>> GetAdmin()
        {
            return await _context.Administrators.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Administrator>> Signup([FromBody] Administrator oUSer)
        {
           
            var admin = _userService.Signup(oUSer);
             _context.Administrators.Add(admin);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdmin", new { id = admin.Id }, admin);


        }


        [HttpPost("Login")]
        public ActionResult<Administrator> GetAdmin([FromBody] Administrator oUSer)
        {
            var account = _context.Administrators.SingleOrDefault(x => x.Email == oUSer.Email);

            bool isValidPassword = BCrypt.Net.BCrypt.Verify(oUSer.PasswordHash, account.PasswordHash);
            if(isValidPassword){
             
              return Ok(account);
            }

          return BadRequest(new { message = "Invalid email or password" });
           
        }
    }
}
 