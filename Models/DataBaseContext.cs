using Microsoft.EntityFrameworkCore;

namespace bingoweb.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
           
        }
         public DbSet<Administrator> Administrators { get; set; }
         public DbSet<Boards> Boards { get; set; }
        
    }
}